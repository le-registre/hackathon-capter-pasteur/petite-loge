**L'idée :**

Création d’une cartographie des besoins et des réponses à ces besoins à l’intérieur du lieu et sur le site internet.

_Plus d'infos sur le wiki : https://gitlab.com/le-registre/hackathon-capter-pasteur/petite-loge/-/wikis/home_
